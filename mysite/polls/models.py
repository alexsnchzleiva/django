from django.db import models

# Create your models here.

class Poll(models.Model):
	"""docstring for Poll"""
	question = models.CharField(max_length=200)

	def __unicode__(self):
		return self.question
		